package main

import (
	"errors"
	"net/http"

	"../../eventbus"

	// "github.com/aleccarper/serverless-eventbus/eventbus"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	ginadapter "github.com/awslabs/aws-lambda-go-api-proxy/gin"
	"github.com/gin-gonic/gin"
)

var initialized = false
var ginLambda *ginadapter.GinLambda

func Handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	if !initialized {
		ginEngine := eventbus.MountAuthorizedRoute("/search", "post", processRequest)
		ginLambda = ginadapter.New(ginEngine)
		initialized = true
	}
	return ginLambda.Proxy(req)
}

type Input struct {
	Movie string `form:"movie" json:"movie" binding:"required"`
}

func processRequest(c *gin.Context) {
	var input Input
	c.BindJSON(&input)
	subscription := eventbus.SearchTorrents(input.Movie)
	c.JSON(http.StatusCreated, subscription)
}
func OnlyErrors() error {
	return errors.New("something went wrong!")
}
func main() {
	lambda.Start(Handler)
}
