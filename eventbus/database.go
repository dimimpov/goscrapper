package eventbus

import (
	"fmt"

	db "../pg"
)

const (
	host     = "terraform-20191030220624278100000001.cui09b5gaslx.us-east-2.rds.amazonaws.com"
	port     = 5432
	user     = "test"
	password = "testtest"
	dbname   = "test"
)

type Torrent struct {
	ID  int    `sql:"type:SERIAL;primary key"`
	Url string `sql:"type:varchar(250);unique"`
}

var newdb db.DB
var err error
var torrent Torrent

func init() {
	newdb, err =
		db.New("postgres") //db.SQLITE // db.MONGODB
	if err != nil {
		panic(err)
	}
	psqlInfo2 := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	newdb.Connect(psqlInfo2)
	// newdb.Table(torrent)
}
