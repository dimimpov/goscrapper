package db

import (
	"database/sql"
	"fmt"
	"github.com/lib/pq"
	"reflect"
	"strings"
)

type Postgres struct {
	db    *sql.DB
	table string
}

func (m *Postgres) Table(table interface{}) bool {
	tableName := pq.QuoteIdentifier(reflect.TypeOf(table).Name())
	val := reflect.ValueOf(table)
	var concatStr []string
	var replacer = strings.NewReplacer(";", " ")
	for i := 0; i < val.NumField(); i++ {
		field := val.Type().Field(i)
		tag := field.Tag

		fieldName := field.Name //get struct variable's name
		replacedTag := replacer.Replace(tag.Get("sql"))
		fieldByTag1 := strings.Split(replacedTag, ":") // get struct tag's name and this should be split by ;
		str := fmt.Sprintf("%s %s", fieldName, strings.Join(fieldByTag1[1:], " "))
		concatStr = append(concatStr, str)
		// fieldByTag2 := tag.Get("your-another-tag") // get another struct tag's name(if you have more)
	}
	querysql := strings.Join(concatStr, ",\n")
	fmt.Println(querysql)
	query := fmt.Sprintf("CREATE TABLE %s(%s);", tableName, querysql)

	_, err := m.db.Query(query)
	if err != nil {
		panic(err)
	}
	return true

}

func (m *Postgres) Connect(psqlinfo string) {
	fmt.Println(psqlinfo)
	db, err := sql.Open("postgres", psqlinfo)
	if err != nil {
		panic(err)
	}
	db.SetMaxIdleConns(100)
	m.db = db
}
func (m *Postgres) Close() {
	defer m.db.Close()

}

func (m *Postgres) Insert(model interface{}) bool {
	object := reflect.TypeOf(model)
	tableName := pq.QuoteIdentifier(reflect.TypeOf(model).Name())
	val := reflect.ValueOf(model)
	var columns []string
	var values []interface{}
	for i := 0; i < val.NumField(); i++ {

		column := object.Field(i).Name //get struct variable's name
		if strings.ToLower(column) != "id" {
			values = append(values, val.Field(i).String())
			columns = append(columns, column)

		}

		// fieldByTag2 := tag.Get("your-another-tag") // get another struct tag's name(if you have more)
	}
	var num_values []string
	for y := 1; y < len(values)+1; y++ {
		value := fmt.Sprintf("$%v", y)
		fmt.Println(value)
		num_values = append(num_values, value)
	}
	// fmt.Println(values)
	// fmt.Println(columns)

	columns_to_str := strings.Join(columns, ",")
	num_of_variables := strings.Join(num_values, ",")

	querysql := fmt.Sprintf("insert into %s (%s) values (%s);", tableName, columns_to_str, num_of_variables)
	fmt.Println(querysql)

	_, err := m.db.Exec(querysql, values...)
	if err != nil {
		panic(err)
	}
	return true

}
