package db

import (
	// "fmt"
	// "database/sql"
	"errors"

	"sync"

	_ "github.com/lib/pq"
)

type DB interface {
	Connect(psqlInfo string)
	Table(model interface{}) bool //set table name
	Close()
	Insert(model interface{}) bool
	// Query(q string) string //perform raw query on the table
	// Find(id int) string    //find a record by it's primary id
	// Get() string           //fetch all the records from the table
}

var db DB          //will be used as a singleton db object
var once sync.Once //make thread safe singleton

func New(dbtype string) (DB, error) {
	var err error
	once.Do(func() {
		db, err = databaseFactory(dbtype)
	})
	return db, err
}
func databaseFactory(driver string) (DB, error) {
	switch driver {
	case "postgres":
		return new(Postgres), nil
	default:
		return nil, errors.New("Unsupported storage driver!")
	}
}

// func Connect(psqlInfo string) *sql.DB {
// 	fmt.Println(psqlInfo)
// 	db, err := sql.Open("postgres", psqlInfo)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer db.Close()
// 	return db
// }

// const (
// 	host     = "localhost"
// 	port     = 5432
// 	user     = "postgres"
// 	password = "postgres"
// 	dbname   = "gotest"
// )

// func main() {
// psqlInfo2 := fmt.Sprintf("host=%s port=%d user=%s "+
// 	"password=%s dbname=%s sslmode=disable",
// 	host, port, user, password, dbname)
// res := db1.Connect(psqlInfo2)
// db1.Table("ddd")
// err := createSchema(db)
// if err != nil {
// 	panic(err)
// }

// }
