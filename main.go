package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/gocolly/colly"
	"golang.org/x/net/html"
)

type Geo struct {
	City   string `json:"city"`
	Region string `json:"region"`
}
type Data struct {
	Ip  string `json:"ip"`
	Geo Geo    `json:"geo"`
}

// type Torrent struct {
// 	ID  int    `sql:"type:SERIAL;primary key"`
// 	Url string `sql:"type:varchar(250);unique"`
// }

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbname   = "gotest"
)

func main() {
	// db1, err1 := db.New("postgres") //db.SQLITE // db.MONGODB
	// if err1 != nil {
	// 	panic(err1)
	// }
	// psqlInfo2 := fmt.Sprintf("host=%s port=%d user=%s "+
	// 	"password=%s dbname=%s sslmode=disable",
	// 	host, port, user, password, dbname)
	// db1.Connect(psqlInfo2)
	// var torrent Torrent
	// db1.Table(torrent)
	// fmt.Println(db1)
	c := colly.NewCollector()
	var myslice []string
	// Find and visit all links
	c.OnHTML("h4 > a", func(e *colly.HTMLElement) {
		myslice = append(myslice, e.Attr("href"))
	})

	c.Visit("https://torrentfreak.com/top-10-most-popular-torrent-sites-of-2019/")

	proxyUrl, err := url.Parse("http://lum-customer-hl_e5c65180-zone-static:kmi8aesc3p32@zproxy.lum-superproxy.io:22225")
	myClient := &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)}}
	resp, err := myClient.Get("http://lumtest.com/myip.json")
	if err != nil {
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	var data Data
	error2 := json.Unmarshal([]byte(body), &data)
	if error2 != nil {
		fmt.Println("whoops:", error2)
	}
	fmt.Println(data)

	fmt.Println(myslice[1])
	torrentUrl := myslice[1] + "/srch?search=avengers"

	respHtml, error3 := myClient.Get(torrentUrl)
	if error3 != nil {
		fmt.Println("whoops:", error2)
	}
	z := html.NewTokenizer(respHtml.Body)

	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			// End of the document, we're done
			return
		case tt == html.StartTagToken:
			t := z.Token()

			isAnchor := t.Data == "a"
			if isAnchor {
				for _, a := range t.Attr {
					if a.Key == "href" {
						fmt.Println("Found href:", a.Val)
						if strings.Contains(a.Val, "1080p") {
							// indiv_torrent := myslice[1] + a.Val
							// torrent.Url = indiv_torrent
							// db1.Insert(torrent)
						}
						break
					}
				}
			}
		}
	}

}
